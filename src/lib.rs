// SPDX-License-Identifier: AGPL-3.0-or-later
// Copyright (C) 2022 Kunal Mehta <legoktm@debian.org>
use serde::{Deserialize, Serialize};
use time::Date;

#[derive(Debug, Serialize, Deserialize)]
pub struct Vote {
    pub date: Date,
    pub username: String,
    pub domain: String,
    pub status: VoteStatus,
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub enum VoteStatus {
    Valid,
    Old,
    Struck,
}

impl From<Option<&str>> for VoteStatus {
    fn from(class: Option<&str>) -> Self {
        match class {
            Some("securepoll-old-vote securepoll-struck-vote") => Self::Struck,
            Some("securepoll-struck-vote") => Self::Struck,
            Some("securepoll-old-vote") => Self::Old,
            Some(val) => panic!("Unknown class: {}", val),
            None => Self::Valid,
        }
    }
}
