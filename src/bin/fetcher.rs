// SPDX-License-Identifier: AGPL-3.0-or-later
// Copyright (C) 2022 Kunal Mehta <legoktm@debian.org>
use anyhow::Result;
use regex::Regex;
use reqwest::Client;
use serde_json::Value;
use std::collections::{HashMap, HashSet};
use time::format_description::FormatItem;
use time::macros::format_description;
use time::Date;
use tokio::fs;
use vote_tracker::{Vote, VoteStatus};

const USER_AGENT: &str = toolforge::user_agent!("vote-tracker");
const FORMAT: &[FormatItem] =
    format_description!("[day padding:none] [month repr:long] [year]");

async fn user_group(
    client: &Client,
    wiki: &str,
    group: &str,
    filename: &str,
) -> Result<Vec<String>> {
    let url  = format!("https://{wiki}/w/api.php?action=query&list=allusers&augroup={group}&aulimit=max&formatversion=2&format=json");
    println!("Fetching {url}...");
    let req = client.get(&url).build()?;
    let resp = client.execute(req).await?;
    let data: Value = resp.error_for_status()?.json().await?;
    let mut users = vec![];
    for item in data["query"]["allusers"].as_array().unwrap() {
        users.push(item["name"].as_str().unwrap().to_string());
    }
    fs::write(filename, serde_json::to_string(&users)?).await?;
    Ok(users)
}

async fn page_links(
    client: &Client,
    wiki: &str,
    page: &str,
    filename: &str,
) -> Result<Vec<String>> {
    let url = format!("https://{wiki}/w/api.php?action=query&format=json&prop=links&titles={page}&formatversion=2&plnamespace=2&pllimit=max&format=json");
    println!("Fetching {url}...");
    let req = client.get(&url).build()?;
    let resp = client.execute(req).await?;
    let data: Value = resp.error_for_status()?.json().await?;
    let mut users = HashSet::new();
    for item in data["query"]["pages"][0]["links"].as_array().unwrap() {
        let title = item["title"]
            .as_str()
            .unwrap()
            .strip_prefix("User:")
            .unwrap()
            .to_string();
        let title = if title.contains('/') {
            title.split_once('/').unwrap().0.to_string()
        } else {
            title
        };
        users.insert(title);
    }
    let mut users: Vec<_> = users.into_iter().collect();
    users.sort_by_key(|x| x.to_string());
    fs::write(filename, serde_json::to_string(&users)?).await?;
    Ok(users)
}

async fn get_votes(client: &Client, election: u64) -> Result<Vec<Vote>> {
    let mut votes = vec![];
    let mut url = format!("https://vote.wikimedia.org/w/index.php?limit=500&title=Special%3ASecurePoll%2Flist%2F{election}");
    loop {
        println!("Fetching {url}...");
        let req = client.get(&url).build()?;
        let resp = client.execute(req).await?;
        let html = resp.error_for_status()?.text().await?;
        let vote_regex = Regex::new(
            r#"<tr( class="(.*?)")?>
<td class="TablePager_col_vote_id">(.*?)</td>
<td class="TablePager_col_vote_voter_name">(.*?)</td>
<td class="TablePager_col_vote_voter_domain">(.*?)</td>
</tr>"#,
        )
        .unwrap();
        for cap in vote_regex.captures_iter(&html) {
            let date = Date::parse(&cap[3], FORMAT)?;
            let vote = Vote {
                date,
                username: cap[4].to_string(),
                domain: cap[5].to_string(),
                status: VoteStatus::from(cap.get(2).map(|val| val.as_str())),
            };
            votes.push(vote);
        }
        let next_regex =
            Regex::new(r#"TablePager-button-next(.*?)href='(.*?)'"#).unwrap();
        match next_regex.captures(&html) {
            Some(cap) => {
                url = format!(
                    "https://vote.wikimedia.org{}",
                    &cap[2].replace("&amp;", "&")
                );
            }
            None => {
                println!("no more vote pages");
                break;
            }
        }
    }
    Ok(votes)
}

async fn is_eligible(client: &Client, username: &str) -> Result<bool> {
    let url =
        format!("https://meta.toolforge.org/accounteligibility/63/{username}");
    println!("Fetching {url}...");
    let req = client.get(&url).build()?;
    let resp = client.execute(req).await?;
    let html = resp.error_for_status()?.text().await?;
    Ok(html.contains("data-is-eligible=\"1\""))
}

async fn update_eligible(client: &Client, users: Vec<String>) -> Result<()> {
    let mut map: HashMap<String, bool> =
        serde_json::from_str(&fs::read_to_string("eligible.json").await?)?;
    let toolforge: Vec<String> = serde_json::from_str(
        &fs::read_to_string("toolforge-eligible.json").await?,
    )?;
    for username in toolforge {
        map.insert(username, true);
    }
    for username in users {
        if map.contains_key(&username) {
            continue;
        }
        let eligible = is_eligible(client, &username).await?;
        map.insert(username, eligible);
    }
    fs::write("eligible.json", serde_json::to_string(&map)?).await?;
    Ok(())
}

#[tokio::main]
async fn main() -> Result<()> {
    let client = Client::builder().user_agent(USER_AGENT).build()?;
    // 2021 data is static, don't need to refetch
    // let votes = get_votes(1079).await?;
    // fs::write("2021.json", serde_json::to_string(&votes)?).await?;
    let votes2 = get_votes(&client, 1364).await?;
    fs::write("2022.json", serde_json::to_string(&votes2)?).await?;
    println!("Wrote 2021 and 2022 files");
    let mut users = vec![];
    users.append(
        &mut user_group(
            &client,
            "meta.wikimedia.org",
            "steward",
            "stewards.json",
        )
        .await?,
    );
    users.append(
        &mut user_group(
            &client,
            "www.mediawiki.org",
            "sysop",
            "mwo-admin.json",
        )
        .await?,
    );
    users.append(
        &mut page_links(
            &client,
            "en.wikipedia.org",
            "Wikipedia:New pages patrol/Coordination/2022 WMF letter",
            "npp-letter.json",
        )
        .await?,
    );
    users.append(
        &mut page_links(
            &client,
            "meta.wikimedia.org",
            "New England Wikimedians/Members",
            "new.json",
        )
        .await?,
    );
    users.append(
        &mut page_links(
            &client,
            "meta.wikimedia.org",
            "Template:@OHmembers",
            "ohio.json",
        )
        .await?,
    );
    for filename in [
        "nyc.json",
        "discord.json",
        "enwp-admins.json",
        "irc.json",
        "enwp-most-edits.json",
    ] {
        users.append(&mut serde_json::from_str(
            &fs::read_to_string(filename).await?,
        )?);
    }
    update_eligible(&client, users).await?;
    println!("Fetched user groups");
    Ok(())
}
