#[macro_use]
extern crate rocket;

use mwtitle::TitleCodec;
use rocket::serde::json::Json;
use rocket_dyn_templates::Template;
use rocket_healthz::Healthz;
use serde::Serialize;
use std::collections::{BTreeMap, HashMap, HashSet};
use time::macros::date;
use time::Date;
use tokio::{fs, sync::OnceCell};
use vote_tracker::{Vote, VoteStatus};

const CURRENT_START: Date = date!(2022 - 08 - 23);
const OLD_START: Date = date!(2021 - 08 - 18);

const LISTS: [[&str; 4]; 11] = [
    [
        "stewards",
        "stewards.json",
        "stewards",
        "meta.wikimedia.org",
    ],
    [
        "mwo-admins",
        "mwo-admin.json",
        "mediawiki.org admins",
        "www.mediawiki.org",
    ],
    [
        "enwp-admins",
        "enwp-admins.json",
        "en.wikipedia.org admins",
        "en.wikipedia.org",
    ],
    ["nyc", "nyc.json", "NYC meetup list", "en.wikipedia.org"],
    [
        "discord",
        "discord.json",
        "[[Category:Wikipedians who use Discord (software)]] list",
        "en.wikipedia.org",
    ],
    [
        "toolforge",
        "toolforge-eligible.json",
        "Toolforge tool maintainers",
        "meta.wikimedia.org",
    ],
    [
        "npp-letter",
        "npp-letter.json",
        "Signers of NPP letter",
        "en.wikipedia.org",
    ],
    [
        "irc",
        "irc.json",
        "[[Category:Wikipedians who use Internet Relay Chat]] list",
        "en.wikipedia.org",
    ],
    [
        "enwp-most-edits",
        "enwp-most-edits.json",
        "[[Wikipedia:List of Wikipedians by number of edits]]",
        "en.wikipedia.org",
    ],
    [
        "new",
        "new.json",
        "New England Wikimedians",
        "meta.wikimedia.org",
    ],
    [
        "ohio",
        "ohio.json",
        "Ohio Wikimedians User Group members",
        "meta.wikimedia.org",
    ],
];

/// The context needed to render the "index.html" template
#[derive(Serialize)]
struct IndexTemplate {
    table: Vec<(i64, usize, usize, String, isize, isize, String)>,
    projects: Vec<(String, usize, String)>,
}

#[derive(Serialize)]
struct ProjectTemplate {
    project: String,
    voters: Vec<String>,
}

#[derive(Serialize)]
struct StaffTemplate {
    voters: Vec<String>,
}

#[derive(Serialize)]
struct ListTemplate {
    has_voted: Vec<String>,
    eligible: Vec<String>,
    remaining: Vec<String>,
    percent: String,
    name: String,
    wiki: String,
    old_votes: HashSet<String>,
}

#[derive(Serialize)]
struct LookupTemplate {
    voted: bool,
    eligible: bool,
    username: String,
}

#[derive(Serialize)]
struct AllTemplate {
    votes: Vec<Vote>,
    old_votes: HashSet<String>,
}

#[derive(Serialize)]
struct StruckTemplate {
    votes: Vec<Vote>,
}

async fn normalize_username(name: &str) -> String {
    static CODEC: OnceCell<TitleCodec> = OnceCell::const_new();
    let codec = CODEC
        .get_or_init(|| async {
            TitleCodec::from_json(
                fs::read_to_string("siteinfo.json")
                    .await
                    .expect("failed to load siteinfo"),
            )
            .expect("failed to build codec")
        })
        .await;
    match codec.new_title(name) {
        Ok(title) => title.dbkey().replace('_', " "),
        Err(_) => name.to_string(),
    }
}

async fn load_votes(year: usize) -> Vec<Vote> {
    serde_json::from_str(
        &fs::read_to_string(format!("{year}.json"))
            .await
            .unwrap_or_else(|err| panic!("failed to read {year}.json: {err}")),
    )
    .unwrap_or_else(|err| panic!("failed to parse {year}.json: {err}"))
}

async fn load_eligible() -> HashMap<String, bool> {
    serde_json::from_str(
        &fs::read_to_string("eligible.json")
            .await
            .expect("failed to read eligible.json"),
    )
    .expect("failed to parse eligible.json")
}

fn valid_votes(votes: Vec<Vote>) -> Vec<Vote> {
    votes
        .into_iter()
        .filter(|vote| matches!(vote.status, VoteStatus::Valid))
        .collect()
}

/// Handle all GET requests for the "/" route. We return either a `Template`
/// instance, or a `Template` instance with a specific HTTP status code.
#[get("/")]
async fn api_index() -> Json<IndexTemplate> {
    let mut projects: HashMap<String, usize> = HashMap::new();
    let mut old_votes: BTreeMap<i64, Vec<String>> = BTreeMap::new();
    for vote in valid_votes(load_votes(2021).await) {
        let day = (vote.date - OLD_START).whole_days();
        old_votes.entry(day).or_default().push(vote.username);
    }
    // A blank Day 15 since 2022 is longer
    old_votes.insert(14, vec![]);
    let mut current_votes: BTreeMap<i64, Vec<String>> = BTreeMap::new();
    for vote in valid_votes(load_votes(2022).await) {
        let day = (vote.date - CURRENT_START).whole_days();
        current_votes.entry(day).or_default().push(vote.username);
        projects
            .entry(vote.domain)
            .and_modify(|val| *val += 1)
            .or_insert(1);
    }
    let mut rows = vec![];
    let mut old_total: isize = 0;
    let mut new_total: isize = 0;
    for (day, votes) in old_votes.into_iter() {
        let old = votes.len();
        let new = current_votes
            .get(&day)
            .map(|votes| votes.len())
            .unwrap_or_default();
        let diff = (new as isize) - (old as isize);
        let diff = if diff > 0 {
            format!("+{diff}")
        } else {
            diff.to_string()
        };
        old_total += old as isize;
        new_total += new as isize;
        let diff_total = new_total - old_total;
        let diff_total = if diff_total > 0 {
            format!("+{diff_total}")
        } else {
            diff_total.to_string()
        };
        rows.push((day + 1, old, new, diff, old_total, new_total, diff_total));
    }
    let mut projects: Vec<_> = projects
        .into_iter()
        .map(|(project, total)| {
            let percent = (total as f64 / new_total as f64) * 100.0;
            (project, total, format!("{:.2}", percent))
        })
        .collect();
    projects.sort_by_key(|k| (k.1, k.0.clone()));
    projects.reverse();
    Json(IndexTemplate {
        table: rows,
        projects,
    })
}

#[get("/")]
async fn index() -> Template {
    Template::render("index", api_index().await.into_inner())
}

#[get("/project/<project>")]
async fn api_project(project: String) -> Json<ProjectTemplate> {
    let votes = valid_votes(load_votes(2022).await);
    let voters: Vec<_> = votes
        .into_iter()
        .filter(|vote| vote.domain == project)
        .map(|vote| vote.username)
        .collect();
    Json(ProjectTemplate { project, voters })
}

#[get("/project/<project>")]
async fn project(project: String) -> Template {
    Template::render("project", api_project(project).await.into_inner())
}

#[get("/staff")]
async fn api_staff() -> Json<StaffTemplate> {
    let votes = valid_votes(load_votes(2022).await);
    let voters: Vec<_> = votes
        .into_iter()
        .filter(|vote| {
            vote.username.ends_with(" (WMF)")
                || vote.username.ends_with("-WMF")
                || vote.username.ends_with(" WMF")
        })
        .map(|vote| vote.username)
        .collect();
    Json(StaffTemplate { voters })
}

#[get("/staff")]
async fn staff() -> Template {
    Template::render("staff", api_staff().await.into_inner())
}

#[get("/all")]
async fn api_all() -> Json<AllTemplate> {
    let mut votes = valid_votes(load_votes(2022).await);
    votes.reverse();
    let old_votes = valid_votes(load_votes(2021).await)
        .into_iter()
        .map(|vote| vote.username)
        .collect();
    Json(AllTemplate { votes, old_votes })
}

#[get("/all")]
async fn all() -> Template {
    Template::render("all", api_all().await.into_inner())
}

#[get("/struck")]
async fn api_struck() -> Json<StruckTemplate> {
    let votes: Vec<_> = load_votes(2022)
        .await
        .into_iter()
        .filter(|vote| matches!(vote.status, VoteStatus::Struck))
        .collect();
    Json(StruckTemplate { votes })
}

#[get("/struck")]
async fn struck() -> Template {
    Template::render("struck", api_struck().await.into_inner())
}

#[get("/lookup?<username>")]
async fn api_lookup(username: String) -> Json<LookupTemplate> {
    let username = normalize_username(&username).await;
    let votes = valid_votes(load_votes(2022).await);
    let voted = votes.iter().any(|vote| vote.username == username);
    let eligible = load_eligible().await;
    let is_eligible = eligible.get(&username).unwrap_or(&false);
    Json(LookupTemplate {
        username,
        voted,
        eligible: *is_eligible,
    })
}

#[get("/lookup?<username>")]
async fn lookup(username: String) -> Template {
    Template::render("lookup", api_lookup(username).await.into_inner())
}

async fn compute_list(
    filename: &str,
    year: usize,
) -> (Vec<String>, Vec<String>, Vec<String>, f64) {
    let users: Vec<String> = serde_json::from_str(
        &fs::read_to_string(filename)
            .await
            .expect("couldn't read group file"),
    )
    .expect("uhoh invalid JSON");
    let voters: HashSet<_> = valid_votes(load_votes(year).await)
        .into_iter()
        .map(|vote| vote.username)
        .collect();
    let eligible_users = load_eligible().await;
    let mut has_voted = vec![];
    let mut eligible = vec![];
    let mut remaining = vec![];
    for user in users {
        if voters.contains(&user) {
            has_voted.push(user);
        } else if *eligible_users.get(&user).unwrap_or(&false) {
            eligible.push(user);
        } else {
            remaining.push(user);
        }
    }

    let percent = has_voted.len() as f64
        / (has_voted.len() + eligible.len()) as f64
        * 100.0;

    (has_voted, eligible, remaining, percent)
}

async fn render_list(
    filename: &str,
    name: &str,
    wiki: &str,
) -> Json<ListTemplate> {
    let (has_voted, eligible, remaining, percent) =
        compute_list(filename, 2022).await;

    let old_votes = valid_votes(load_votes(2021).await)
        .into_iter()
        .map(|vote| vote.username)
        .collect();

    Json(ListTemplate {
        has_voted,
        eligible,
        remaining,
        percent: format!("{:.2}", percent),
        name: name.to_string(),
        wiki: wiki.to_string(),
        old_votes,
    })
}

#[derive(Serialize)]
struct ListIndexTemplate {
    lists: Vec<(&'static str, &'static str, String, String, String)>,
}

#[get("/list")]
async fn api_list_index() -> Json<ListIndexTemplate> {
    let mut data = vec![];
    for list in LISTS {
        let (_, _, _, percent) = compute_list(list[1], 2022).await;
        let (_, _, _, old_percent) = compute_list(list[1], 2021).await;
        let diff = format!("{:.2}", percent - old_percent);
        data.push((
            list[0],
            list[2],
            format!("{:.2}", old_percent),
            format!("{:.2}", percent),
            diff,
        ));
    }
    Json(ListIndexTemplate { lists: data })
}

#[get("/list")]
async fn list_index() -> Template {
    Template::render("list_index", api_list_index().await.into_inner())
}

#[get("/list/<name>")]
async fn api_list(name: String) -> Json<ListTemplate> {
    for list in LISTS {
        if name == list[0] {
            return render_list(list[1], list[2], list[3]).await;
        }
    }
    panic!("Unknown list")
}

#[get("/list/<name>")]
async fn list(name: String) -> Template {
    Template::render("list", api_list(name).await.into_inner())
}

#[launch]
fn rocket() -> _ {
    rocket::build()
        .mount(
            "/",
            routes![
                all, index, list, list_index, lookup, project, staff, struck,
            ],
        )
        .mount(
            "/api/",
            routes![
                api_all,
                api_index,
                api_list,
                api_list_index,
                api_lookup,
                api_project,
                api_staff,
                api_struck,
            ],
        )
        .attach(Template::fairing())
        .attach(Healthz::fairing())
}
